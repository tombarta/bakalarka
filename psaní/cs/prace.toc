\contentsline {chapter}{\IeC {\'U}vod}{2}{chapter*.2}
\contentsline {chapter}{\numberline {1}Slunce}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Pro\IeC {\v c} ho zkoum\IeC {\'a}me?}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Stavba Slunce}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Magnetick\IeC {\'e} pole Slunce}{3}{section.1.3}
\contentsline {chapter}{\numberline {2}Slune\IeC {\v c}n\IeC {\'\i } skvrny}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Vznik slune\IeC {\v c}n\IeC {\'\i }ch skvrn}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Struktura slune\IeC {\v c}n\IeC {\'\i }ch skvrn}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Wilsonova deprese}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Jemn\IeC {\'a} struktura}{5}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Teoretick\IeC {\'e} modely slune\IeC {\v c}n\IeC {\'\i }ch skvrn}{6}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Self-similar modely}{6}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Dynamick\IeC {\'a} diskonexe}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Model dynamick\IeC {\'e} diskonexe a model konvekce}{8}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Model konvekce}{8}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Magnetohydrostatick\IeC {\'a} rovnov\IeC {\'a}ha}{10}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}\IeC {\v C}asov\IeC {\'y} v\IeC {\'y}voj}{11}{subsection.3.1.3}
\contentsline {chapter}{\numberline {4}Simulace dynamick\IeC {\'e} diskonexe}{12}{chapter.4}
\contentsline {section}{\numberline {4.1}Model klidn\IeC {\'e}ho Slunce}{12}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Rovnice p\IeC {\v r}enosu energie}{12}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Hydrostatick\IeC {\'a} rovnov\IeC {\'a}ha}{12}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Stavov\IeC {\'a} rovnice}{13}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Numerick\IeC {\'e} \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{13}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}V\IeC {\'y}sledky}{13}{subsection.4.1.5}
\contentsline {chapter}{Z\IeC {\'a}v\IeC {\v e}r}{16}{chapter*.3}
\contentsline {chapter}{Seznam pou\IeC {\v z}it\IeC {\'e} literatury}{17}{chapter*.4}
\contentsline {chapter}{Seznam tabulek}{18}{chapter*.5}
\contentsline {chapter}{Seznam pou\IeC {\v z}it\IeC {\'y}ch zkratek}{19}{chapter*.6}
\contentsline {chapter}{P\IeC {\v r}\IeC {\'\i }lohy}{20}{chapter*.7}
