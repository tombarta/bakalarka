from scipy.interpolate import UnivariateSpline
from scipy.integrate import odeint
import pandas as pd
import numpy as np
from scipy import sparse
from scipy.sparse.linalg import spsolve
from scipy.interpolate import UnivariateSpline
from scipy.signal import savgol_filter

import pdb

from .common import derivatives as D
from .common.integrals import getP
from .common.const import *
from .ext_data import g
from .OPAL.opacity import opac
from .magnetic import comp_mag
from .pressure_adjustment import adjust_baseP

def disconnection_check(y):
    B = y*y
    return False

def derivative(arr, z):
    try:
        arr = arr.values
    except:
        pass
    try:
        z = z.values
    except:
        pass
    h = z[1]-z[0]
    return savgol_filter(x=arr, window_length=7, polyorder=2, deriv=1, delta=h, mode='interp')
#    func = UnivariateSpline(z, arr, k=1, ext=3, s=0)
#    dfdz = (-func(z+2*h)+8*func(z+h)-8*func(z-h)+func(z-2*h))/(12*h)
#
#    return dfdz
#    return np.array( [(arr[1]-arr[0])/h]+[(arr[i+2]-arr[i])/(2*h) for i in range(0,len(arr)-2)]+[(arr[-1]-arr[-2])/h] )

tau = 15

def solveTeq(old, extrap):
    d_main = np.zeros(501)
    d_sub = np.zeros(501)
    d_sup = np.zeros(501)
    b = np.zeros(501)
    
    h = 25000.	#grid spacing
    
#    A = extrap['T']**3*(16.*sigma/3.)*derivative(1/(extrap['opac']*extrap['rho']))+extrap['T']**2*16*sigma/(extrap['opac']*extrap['rho'])
    B = 16*sigma*extrap['T']**3/(3*extrap['opac']*extrap['rho'])
    A = derivative(B, extrap['z'])
    H = extrap['P'].values/(extrap['rho'].values*g(extrap['z']))
    
    d_main = (-2*B/(h*h)-extrap['rho']*extrap['cp']/tau).values
#    d_main[0] = -(3./2)*A[0]/h+B[0]/(h*h)-extrap['rho'].values[0]*extrap['cp'].values[0]/tau
#    d_main[0] = B[0]-extrap['rho'].values[0]*extrap['cp'].values[0]/tau
    d_main[-1] = 1/h

#    atmT = 2*extrap['T'].values[0]-extrap['T'].values[1]
    atmT = 3500.

    d_sub = (-0.5*A/h+B/(h*h)).values
    b[0] = -d_sub[0]*atmT
    d_sub = d_sub[1:]
    d_sub[-1] = -1/h
    
#    d_sup = (0.5*A/h+B).values
    d_sup = (0.5*A/h+B/(h*h)).values
    d_sup = d_sup[:-1]
#    d_sup[0] = 2*A[0]/h-2*B[0]
    
#    d_sup2 = np.zeros(499)
#    d_sup2[0] = -4*A[0]/h+B[0]
#    d_sup2[0] = B[0]
    
    b = (derivative(extrap['fconv'], extrap['z'])-extrap['rho']*extrap['cp']*old['T']/tau).values
    b[-1] = extrap['nabla_ad'].values[-1]*extrap['T'].values[-1]/H[-1]
#    C0 = extrap['T'].values[0]-3500
#    C1 = np.log((extrap['T'].values[1]-3500.)/C0)/25000.
#    atmT = C0*np.exp(-25000.*C1)+3500.
#    b[0] += 0.5*A[0]*atmT/h-B[0]*atmT/(h*h)
    
#    data = [d_sub, d_main, d_sup, d_sup2]
    d_main[0] = 1
    d_sup[0] = 0
    b[0] = 3500
    data = [d_sub, d_main, d_sup]
    diags = [-1,0,1]


    A = sparse.diags(data, diags, shape=(501, 501), format='csc')
    return spsolve(A,b)


model = pd.read_csv('dynamic_disconnection/data/initial_state.dat', delimiter='\t')
#model = pd.read_csv('dynamic_disconnection/data/vyvoj541.dat', delimiter='\t')
bcg = pd.read_csv('dynamic_disconnection/data/bcg.dat', delimiter='\t')

fixT = lambda x: max(x, 3500)
fixT = np.vectorize(fixT)

#while disconnection_check(y) == False:
T = 0

model['opac'] = model.apply(lambda x: opac(x['T'], x['rho']), axis=1)
extrap = model.copy()
extrap = D.flux(extrap, 0.3)
old = model.copy()
new = model.copy()
new['T'] = solveTeq(old, extrap)
new.to_csv('dynamic_disconnection/data/vyvoj0.dat', sep='\t', index=False)

for i in range(1,1000):
    B_bottom = np.sqrt(8e-7*np.pi*(bcg['P'].values[-1]-new['P'].values[-1]))
    y = comp_mag(np.linspace(Btop, B_bottom, 501), new['P'].values)
    new['y'] = y
    P0 = adjust_baseP(y, new, tau)
    new = getP(P0, UnivariateSpline(new['z'].values, new['T'].values, k=1, s=0, ext=3))
    new.reset_index(drop=True, inplace=True)
    extrap = new.copy()
    old = new.copy()


    extrap['opac'] = extrap.apply(lambda x: opac(x['T'], x['rho']), axis=1)
    extrap = D.flux(extrap, 0.3)
    new['T'] = solveTeq(old, extrap)
#    model = D.flux(model, 0.3)
#    new_model = model.copy()
#    new_model['T'] = model['T']+dt*model['dT']
#    err = (model['T'].values-new_model['T'].values)/model['T'].values
#    max_err = np.abs(err).max()
#    if max_err>0.01:
#        dt = dt/(max_err/0.005)
#    elif max_err<0.001:
#        dt = dt*3

    new['opac'] = new.apply(lambda x: opac(x['T'], x['rho']), axis=1)
    new = D.flux(new, 0.3)
    new['T'] = fixT(new['T'])
    print i
#    model = new_model.copy()
    
    name_str = 'vyvoj'+str(i)+'.dat'
    new.to_csv('dynamic_disconnection/data/'+name_str, sep='\t', index=False)
