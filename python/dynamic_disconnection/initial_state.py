# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from scipy.optimize import bisect
from scipy.integrate import ode

from . import ext_data as ext
from .OPAL.EOS import gas_state

P_bottom = 0.9*ext.P(12.5e6)


def TofS(P0, S0, T0):
	import pdb
	pdb.set_trace()
	return bisect(lambda T: S0-gas_state(T,P0)['S'], 6000, 15000)

ext_model = pd.read_csv('dynamic_disconnection/data/bcg.dat', delimiter='\t')

P_top = 0.5*ext_model['P'].values[0]
S_bottom = ext_model['S'].values[-1]
T_top = TofS(P_top, S_bottom, ext_model['T'].values[0])
import pdb

#====================integrace======================
model = []
def f(z, y):
	T, P = y
	state = gas_state(T, P)
	H = P/(state['rho']*ext.g(z))
	return np.array([ state['nabla_ad']*T/H, P/H ])
	
r = ode(f).set_integrator('dopri5')
y0 = np.array([ T_top, P_top ])
r.set_initial_value(y0, 0)
dz = 2.5e4
while r.successful() and r.t<=12.5e6:
	T = r.y[0]
	P = r.y[1]
	z = r.t
	new_state = np.concatenate(( [z], [T], [P], gas_state(T,P).values() ))
	model.append(new_state.tolist())
	r.integrate(z+dz)

model = np.asarray(model)
model = pd.DataFrame(columns = ['z', 'T', 'P', 'nabla_ad',
				'S', 'rho', 'delta', 'cp', 'cv'], data=model)
model.to_csv('dynamic_disconnection/data/initial_state.dat', sep='\t', index=False)
