# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from scipy import sparse
from scipy.sparse.linalg import spsolve
from scipy.interpolate import UnivariateSpline

from .common.const import Btop, phi

bcg = pd.read_csv('dynamic_disconnection/data/bcg.dat', delimiter='\t')
bcg_P = UnivariateSpline(bcg.z, bcg.P, k=1)(np.linspace(0,12.5e6,501))

def mag_iteration(y, P, eps):
	d_main = np.empty(501)
	d_sub = np.zeros(501)
	d_sup = np.zeros(501)
	b = np.zeros(501)

	h = 25000	#grid spacing

	import pdb
	try:
		Bz0 = np.sqrt(8e-7*np.pi*(bcg_P[500]-P[500]))
	except:
		pdb.set_trace()
	b[0] = np.sqrt(Btop)
	b[500] = np.sqrt(Bz0)

	for i in range(1,500): d_main[i] = y[i]**3+eps*((phi/np.pi)*y[i]/h**2+y[i]**3)
	for i in range(0,499): d_sub[i] = -(eps*phi*y[i+1])/(2*np.pi*h*h)
	for i in range(2,501): d_sup[i] = -(eps*phi*y[i-1])/(2*np.pi*h*h)
	for i in range(1,500): b[i] = y[i]**4+(eps*8e-7*np.pi*(bcg_P[i]-P[i]))
	d_main[0] = 1
	d_main[500] = 1

	data = [d_sub, d_main, d_sup]
	diags = [-1,0,1]
	A = sparse.spdiags(data, diags, 501, 501, format='csc')

	return spsolve(A,b)

def error(v, w):
	return np.linalg.norm(v-w)/np.linalg.norm(v)

def comp_mag(y, P):
	err = 1
	y_new = y
	while err>1e-2:
		err_diff = 2
		i = 0
		err_new = err
		
		while err_new >= err:
			y_new = mag_iteration(y, P, 2**(-i))
			err_new = error(y, y_new)
			i = i+1

		y = y_new
		err = err_new
	return y_new
