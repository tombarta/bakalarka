# -*- coding: utf-8 -*-
from os.path import dirname, realpath, sep, pardir
import sys
sys.path.append(dirname(realpath(__file__)) + sep + pardir + sep + "lib")

import numpy as np

from scipy.optimize import newton
from scipy.interpolate import UnivariateSpline
from .const import *
from ..OPAL.EOS import gas_state
from ..OPAL.opacity import opac
from ..ext_data import g

#   derivace teploty
#__________________________________________________________________


def nabla_conv(U, radG, adG):
    W = radG-adG
    A = newton(lambda xi: (xi-U)**3+(8*U/9)*(xi**2-U**2-W), adG, lambda xi: 3*(xi-U)**2+2*8*U*xi/9)
    return A**2-U**2+adG

def nabla(T, P, z):
    state = gas_state(T,P)
    rho = state['rho']
    nabla_ad = state['nabla_ad']
    kappa = opac(T, rho)
    nabla_rad = (3./64)*(kappa*P*4e26/(2e30*np.pi*G*sigma*T**4))
    if nabla_rad<nabla_ad:
        return nabla_rad
    else:
        H = P/(rho*g(z)) 
        cp = state['cp']
        U = 12*(sigma*T**3/(cp*rho**2*kappa*H**2))*(1/alpha)**2*np.sqrt(8*H/g(z))
        Gc = messyTDeriv(U, nabla_rad, nabla_ad)
        return Gc

#   derivace tlaku
#__________________________________________________________________

def PDeriv(P, z, T):
    r = rho(T, P, z)
    D = r*g(z) 
    return D



#    toky energie
#_______________________________________________________

from scipy.interpolate import UnivariateSpline
from scipy.signal import savgol_filter
ceil = lambda x: min(x, 0)
ceil = np.vectorize(ceil)

def flux(model, alpha):
    z = model['z'].values
    T = model['T'].values
    P = model['P'].values
    kappa = model['opac'].values
    delta = model['delta'].values
    cp = model['cp'].values
    rho = model['rho'].values
    nabla_ad = model['nabla_ad'].values
    H = P/(rho*g(z))
#    model['H'] = H
#    model['delta'] = delta
#    Tfunc = UnivariateSpline(z, T, k=2)
    C0 = T[0]-3500
    C1 = np.log((T[1]-3500.)/C0)/25000.
    atmT = C0*np.exp(-25000.*C1)+3500.
#    atmT = 2*T[0]-T[1]
#    atmT = 3500.

#    dTdz = np.array( [(T[1]-T[0])/(z[1]-z[0])]+[ (T[i+2]-T[i])/(z[i+2]-z[i]) for i in range(0,len(T)-2) ]+[nabla_ad[-1]*T[-1]/H[-1]] )
    Tfunc = UnivariateSpline(z, T, k=1, ext=0)
    h = z[1]-z[0]
    dTdz = (-Tfunc(z+2*h)+8*Tfunc(z+h)-8*Tfunc(z-h)+Tfunc(z-2*h))/(12*h)
    dTdz2 = (Tfunc(z+h)-Tfunc(z-h))/(2*h)
#    dTdz = np.append(np.diff(T)/np.diff(z), T[-1]*nabla_ad[-1]/H[-1])
#    dTdz = Tfunc.derivative()(z)
    dTdz = savgol_filter(x=T, window_length=9, polyorder=2, deriv=1, delta=h, mode='interp')
    nabla = (H/T)*dTdz
    model['nabla'] = nabla
#    model['dtdz'] = dTdz
#    model['nabla'] = nabla
#    model['g'] = g(z)
    nabla_rad = (3./64)*(kappa*P*4e26/(2e30*np.pi*G*sigma*T**4))

    frad = -(16./3.)*sigma*g(z)*T**3*dTdz/(kappa*rho)
    fix = lambda x: max(x,0)
    fix = np.vectorize(fix)
#    frad = fix(frad)
#    frad[:np.argmax(frad)] = np.max(frad)
    model['frad'] = frad

    U = 12*(sigma*T**3/(cp*rho**2*kappa*H**2))*(1/alpha)**2*np.sqrt(8*H/g(z))
    model['U'] = U
#    model['U'] = U
    nabla_e = nabla_ad-2*U**2+2*U*np.sqrt(abs(nabla-nabla_ad)+U**2)
    model['nabla_e'] = nabla_e
#    model['nable_e'] = nabla_e
    
    def myround(x):
            return x if x>0.01 else 0

    myround = np.vectorize(myround, otypes=[np.float])

    fconv = -rho*cp*T*np.sqrt(g(z)*delta)*alpha**2*np.sqrt(H)*(myround(nabla-nabla_e))**1.5/(4*np.sqrt(2.))
    fconv[T<5000] = 0
#    fconv = 0
    model['fconv'] = fconv

    model['ftot'] = (16./3.)*sigma*g(z)*T**4*nabla_rad/(kappa*P)

    f = frad+fconv
    model['dT'] = ceil(np.array( [(f[1]-f[0])/(z[1]-z[0])]+[ (f[i+2]-f[i])/(z[i+2]-z[i]) for i in range(0,len(f)-2) ]+[(f[-1]-f[-2])/(z[-1]-z[-2])] )/(rho*cp))

    return model
