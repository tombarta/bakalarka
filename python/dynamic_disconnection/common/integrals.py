from scipy.integrate import ode
import numpy as np
import pandas as pd

from .. import ext_data as ext
from ..OPAL.EOS import gas_state

import sys

def getP(P_bottom, Tfunc):
    import pdb
    model = []

    def f(z, P):
        T = Tfunc(z)
#        if (T<5000) and (P<100):
#                print T, P
#                sys.stdout.flush()
        state = gas_state(T, P)
        H = P/(state['rho']*ext.g(z))
        return P/H
        
    r = ode(f).set_integrator('vode')
    y0 = np.array([ P_bottom ])
    r.set_initial_value(y0, 12.5e6)
    dz = 2.5e4
    while r.successful() and r.t>=0:
        P = r.y[0]
        z = r.t
        T = Tfunc(z).item()
        new_state = np.concatenate(( [z], [T], [P] ))
        model.append(new_state.tolist())
        r.integrate(z-dz)
    model = np.asarray(model)
    model = pd.DataFrame(columns = ['z', 'T', 'P'], data=model)
#    model_eos = pd.DataFrame(columns = ['nabla_ad', 'S', 'rho', 'delta', 'cp', 'cv'])
    model_eos = model.apply(lambda x: pd.Series(gas_state(x['T'],x['P'])), axis=1)
    model = model.join(model_eos)
    return model.sort_index(by='z')
