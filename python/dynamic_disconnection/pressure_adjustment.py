import numpy as np
import pandas as pd
from scipy import optimize, integrate, interpolate

from .common.const import *
from .common.integrals import getP
from .ext_data import g
from .magnetic import comp_mag
from .OPAL.EOS import gas_state

bcg = pd.read_csv('dynamic_disconnection/data/bcg.dat', delimiter='\t')

import pdb

def mass(P0, y, model):
    z = model['z'].values
    rho = model['rho'].values
    model = getP(P0, interpolate.UnivariateSpline(model['z'].values, model['T'].values, k=1, ext=3))
    # interpolace pri extrapolaci vraci krajni hodnotu

    B_bottom = np.sqrt(8e-7*np.pi*(bcg['P'].values[-1]-model['P'].values[-1]))
    y = comp_mag(np.linspace(Btop, B_bottom, 501), model['P'].values)
    B = y*y

    mmbrs = np.zeros(500)

    for i in range(500):
        mmbrs[i] = (rho[i]/B[i]+rho[i+1]/B[i+1])*(z[i+1]-z[i])/2
    integral = mmbrs.sum()
    return phi*integral

def adjust_baseP(y, model, dt, known_mass = 0, static={"known_mass":0}):
    T = model['T'].values
    z = model['z'].values
    P0 = model['P'].values[-1]
    def rho(T, P):
        return gas_state(T, P)['rho']
    known_mass = mass(P0, y, model)
    return optimize.brentq(lambda P: mass(P, y, model)-known_mass-phi*rho(T[-1],P)*v0*dt/(y[-1]*y[-1]), model['P'].values[-1], bcg['P'].values[-1], rtol=1e-10)
#    return optimize.newton(lambda P: mass(P, y, model)-known_mass-phi*rho(T[-1],P)*v0*dt/(y[-1]*y[-1]), model['P'].values[-1])
